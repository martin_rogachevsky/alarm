package mrog.alarm;


import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.util.Random;

import alexander.alarmclock.R;
import mrog.alarm.MainActivity;


public class RingtonePlayingService extends Service {

    MediaPlayer media_song;
    int startId;
    boolean isRunning;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        String state = intent.getExtras().getString("extra");
        Integer whale_sound_choice = intent.getExtras().getInt("whale_choice");

        Log.e("Ringtone extra is ", state);
        Log.e("Whale choice is ", whale_sound_choice.toString());

        NotificationManager notify_manager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        Intent intent_main_activity = new Intent(this.getApplicationContext(), MainActivity.class);
        PendingIntent pending_intent_main_activity = PendingIntent.getActivity(this, 0,
                intent_main_activity, 0);

        Notification notification_popup = new Notification.Builder(this)
                .setContentTitle("An alarm is going off!")
                .setContentText("Click me!")
                .setContentIntent(pending_intent_main_activity)
                .setAutoCancel(true)
                .build();

        assert state != null;
        switch (state) {
            case "alarm on":
                startId = 1;
                break;
            case "alarm off":
                startId = 0;
                Log.e("Start ID is ", state);
                break;
            default:
                startId = 0;
                break;
        }

        if (!this.isRunning && startId == 1) {
            Log.e("there is no music, ", "and you want start");
            this.isRunning = true;
            this.startId = 0;
            if (notify_manager != null) {
                notify_manager.notify(0, notification_popup);
            }
            if (whale_sound_choice == 0) {
                int minimum_number = 1;
                int maximum_number = 13;

                Random random_number = new Random();
                int whale_number = random_number.nextInt(maximum_number + minimum_number);
                Log.e("random number is ", String.valueOf(whale_number));
                select_ringtone(whale_number);
            } else {
                select_ringtone(whale_sound_choice);
            }
        } else if (this.isRunning && startId == 0) {
            Log.e("there is music, ", "and you want end");
            media_song.stop();
            media_song.reset();
            this.isRunning = false;
            this.startId = 0;
        } else if (!this.isRunning) {
            Log.e("there is no music, ", "and you want end");
            this.isRunning = false;
            this.startId = 0;
        } else {
            Log.e("there is music, ", "and you want start");
            this.isRunning = true;
            this.startId = 1;
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.e("on Destroy called", "ta da");
        super.onDestroy();
        this.isRunning = false;
    }

    private void select_ringtone(int whale_sound_choice) {
        switch (whale_sound_choice) {
            case 1: {
                media_song = MediaPlayer.create(this, R.raw.bubblenet_and_vocals);
                break;
            }
            case 2: {
                media_song = MediaPlayer.create(this, R.raw.contact_call_moo);
                break;
            }
            case 3: {
                media_song = MediaPlayer.create(this, R.raw.contact_call_whup);
                break;
            }
            case 4: {
                media_song = MediaPlayer.create(this, R.raw.feeding_call);
                break;
            }
            case 5: {
                media_song = MediaPlayer.create(this, R.raw.flipper_splash);
                break;
            }
            case 6: {
                media_song = MediaPlayer.create(this, R.raw.tail_slaps_with_propeller_whine);
                break;
            }
            case 7: {
                media_song = MediaPlayer.create(this, R.raw.whale_song);

                break;
            }
            case 8: {
                media_song = MediaPlayer.create(this, R.raw.whale_song_with_outboard_engine_noise);
                break;
            }
            case 9: {
                media_song = MediaPlayer.create(this, R.raw.wheeze_blows);
                break;
            }
            case 10: {
                media_song = MediaPlayer.create(this, R.raw.echolocation_clicks);
                break;
            }
            case 11: {
                media_song = MediaPlayer.create(this, R.raw.offshore);
                break;
            }
            case 12: {
                media_song = MediaPlayer.create(this, R.raw.resident);
                break;
            }
            case 13: {
                media_song = MediaPlayer.create(this, R.raw.song_transient);
                break;
            }
            default: {
                media_song = MediaPlayer.create(this, R.raw.resident);
                break;
            }
        }
        media_song.start();
    }
}
