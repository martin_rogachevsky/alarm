package mrog.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("We are in the receiver.", "Yay!");
        String get_your_string = intent.getExtras().getString("extra");
        Log.e("What is the key? ", get_your_string);
        Integer get_your_whale_choice = intent.getExtras().getInt("whale_choice");
        Log.e("The whale choice is ", get_your_whale_choice.toString());
        Intent service_intent = new Intent(context, RingtonePlayingService.class);
        service_intent.putExtra("extra", get_your_string);
        service_intent.putExtra("whale_choice", get_your_whale_choice);
        context.startService(service_intent);
    }
}
